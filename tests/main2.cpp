//
// Created by hemil on 04/07/21.
//

#include <stfu/stfu.h>
#include "test_util.h"

static EnsureUsed ensureUsed;

static int dummy = stfu::test("test in main2.cpp", [] {
    ensureUsed.use();
});