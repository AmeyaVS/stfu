//
// Created by hemil on 11/09/21.
//

#define STFU_IMPL
#include <stfu/stfu.h>

class BankAccount {
    int balance;
public:
    explicit BankAccount(int balance) : balance(balance) {}

    void deposit(int amount) {
        balance += amount;
    }

    void withdraw(int amount) {
        balance -= amount;
    }

    int current_balance() const {
        return balance;
    }
};

int main() {
    stfu::test("Bank account tests", [] {
        BankAccount bankAccount(100);

        stfu::test("Depositing 100 bucks changes current balance to 200", [&] {
            expect(bankAccount.current_balance() == 100);
            bankAccount.deposit(100);
            expect(bankAccount.current_balance() == 200);
        });

        stfu::test("Withdrawing 100 bucks changes current balance to 0", [&] {
            expect(bankAccount.current_balance() == 100);
            bankAccount.withdraw(100);
            expect(bankAccount.current_balance() == 0);
        });
    });
}